package id.artivisi.training.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.artivisi.training.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {

}
